const body = document.querySelector('body')
const mainHeader = document.querySelector('.main-header')

function handleScroll() {
  if (window.scrollY >= 100) {
    body.classList.add('scrolling')
    mainHeader.classList.add('main-header--scrolling')
  } else {
    body.classList.remove('scrolling')
    mainHeader.classList.remove('main-header--scrolling')
  }
}

document.addEventListener('scroll', handleScroll)
window.addEventListener('load', handleScroll)

// main menu
const menuToggle = document.querySelector('.js-menu-toggle')
const mainMenu = document.querySelector('.main-menu')

menuToggle.addEventListener('click', function (e) {
  e.preventDefault()
  menuToggle.classList.toggle('hamburger-icon--active')
  mainMenu.classList.toggle('main-menu--open')
})


// sidebar
const sidebarToggle = document.querySelector('.js-sidebar-toggle')
const sidebar = document.querySelector('.main-footer')

sidebarToggle.addEventListener('click', function (e) {
  e.preventDefault()
  sidebarToggle.classList.toggle('hamburger-icon--invert')
  sidebarToggle.classList.toggle('hamburger-icon--active')
  sidebar.classList.toggle('main-footer--open')
})

// video player
const playBtn = document.querySelector('.js-play-trailer')
const trailer = document.querySelector('.trailer')
const closeTrailerBtn = document.querySelector('.trailer_close-btn')
playBtn.addEventListener('click', function (e) {
  e.preventDefault()
  trailer.classList.add('trailer--active')
  body.classList.add('no-scroll')
  player.playVideo();
})
closeTrailerBtn.addEventListener('click', function (e) {
  e.preventDefault()
  trailer.classList.remove('trailer--active')
  body.classList.remove('no-scroll')
  player.pauseVideo();
})


let player;
// Link https://youtu.be/ObmyyKoKe0U
function onYouTubeIframeAPIReady() {
  player = new YT.Player('video-placeholder', {
    width: 600,
    height: 400,
    videoId: 'ObmyyKoKe0U',
    playerVars: {
      color: 'white'
    },
    events: {
      onReady: initialize
    }
  });
}
function initialize() {

}


// menu click (scrol smoothly)
const smoothLinks = document.querySelectorAll('.scroll-smoothly')
for (const link of smoothLinks) {
  link.addEventListener('click', handleMenuClick)
}

function handleMenuClick(event) {
  event.preventDefault()
  const href = event.target.getAttribute('href')
  const target = document.querySelector(href)
  console.log(event)
  scrollTo(target)
}

// scrol smoothly
function scrollTo(element, offset = -100) {
  window.scroll({
    behavior: 'smooth',
    left: 0,
    top: element.offsetTop + offset
  });
}

// load youtube
window.addEventListener('load', function () {
  const tag = document.createElement("script")
  tag.src = "https://www.youtube.com/iframe_api"
  document.querySelector('body').appendChild(tag)
})

// lazy load
document.addEventListener("DOMContentLoaded", function () {
  const lazyloadImages = document.querySelectorAll(".lazy-img")
  let lazyloadThrottleTimeout = null

  function lazyload() {
    if (lazyloadThrottleTimeout) {
      clearTimeout(lazyloadThrottleTimeout)
    }

    lazyloadThrottleTimeout = setTimeout(function () {
      const scrollTop = window.pageYOffset
      lazyloadImages.forEach(function (img) {
        const rect = img.getBoundingClientRect()
        if (rect.top < (window.innerHeight + scrollTop) && !img.dataset.loaded) {
          img.src = img.dataset.src
          img.dataset.loaded = true
          img.classList.remove('lazy-img')
        }
      });
      if (lazyloadImages.length == 0) {
        document.removeEventListener("scroll", lazyload)
        window.removeEventListener("resize", lazyload)
        window.removeEventListener("orientationChange", lazyload)
      }
    }, 20)
  }

  document.addEventListener("scroll", lazyload)
  window.addEventListener("resize", lazyload)
  window.addEventListener("orientationChange", lazyload)
  lazyload()
});

// Nodelist polyfill
if ('NodeList' in window && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window;
    for (let i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this)
    }
  };
}

document.addEventListener('DOMContentLoaded', function () {
  new Splide('.splide', {
    perPage: 4,
    perMove: 1,
    arrows: false,
    autoplay: true,
    cover: true,
    focus: 'center',
    trimSpace: true,
    pagination: false,
    rewind: true,
    breakpoints: {
      640: {
        perPage: 2
      }
    }
  }).mount();
})